tabuadaASerPulada = undefined;
tabuadaProibida   = undefined;
tabuadasGeradas = [];

var armazenaHistorico = function(historico){
  if(tabuadasGeradas.length == 10){
    tabuadasGeradas.shift();
  }

  tabuadasGeradas.push(historico);
}

var apresentaHistorico =  function(){
  var historico = "";

  if(tabuadasGeradas.length == 0){
    historico = "Não há históricos a serem apresentados";
  }
  else{
    for(var i = 0; i < tabuadasGeradas.length; i++){
      historico += (i+1) + "º = " + tabuadasGeradas[i] + "\n";
    }
  }

  alert(historico);
} 

var pegarNumero = function (textoDoDialogo) {
   var texto  = prompt(textoDoDialogo);
   var numero = parseInt(texto);

   return numero;
}

var gerarTabuada = function(){
  $("#tabuadas").empty(); // Limpando o container que apresenta o código HTML das tabuadas

  var opcao = pegarNumero("Escreva (1) para gerar com um intervalo ou \nEscreva (2) com números determinados");

  if(opcao == 1){
    tabuadaComIntervalo();
  }
  else if(opcao == 2){
    tabuadaComNumerosDeterminados();
  }
  else{
    alert("Escolha entre a opção 1 e 2");
  }
}

var tituloDaTabuada = function(numero){
  var titulo = $("<h1>Tabuada do " + numero + "</h1>");
      titulo.fadeIn(1000);

  $("#tabuadas").append(titulo);
}

var resultadosDaTabuada = function(numero){
  var tabela = $("<table>");

  for(base = 1; base <= 10; base++){
    var resultado = resultadoDaTabuada(numero, base);

    tabela.append(resultado)
  }

  tabela.fadeIn(2000);

  $("#tabuadas").append(tabela);
}

var resultadoDaTabuada = function (numero, base) {
  var resultado = "<tr>" +
                    "<td>" + numero + "</td>" +
                    "<td> x </td>" + 
                    "<td>" + base + "</td>" +
                    "<td> = </td>" +
                    "<td>" + (numero*base) + "</td>" +
                  "</tr>";

  return resultado;
}

var pegarTabuadaASerPulada = function(){
  tabuadaASerPulada = pegarNumero("Digite a tabuada a ser pulada");
}

var pegarTabuadaProibida = function(){
  tabuadaProibida = pegarNumero("Digite um número que irá encerrar o tabuada");
}

var tabuadaComIntervalo = function(){
  var numeroInicial = pegarNumero("Digite o número inicial");
  var numerofinal   = pegarNumero("Digite o número final");
  var historico     = numeroInicial + " ao " + numerofinal; 

  armazenaHistorico(historico);

  for(var numero = numeroInicial; numero <= numerofinal; numero++){
    if(numero == tabuadaASerPulada){
      continue;
    } else if(numero == tabuadaProibida){
      break;
    }

    tituloDaTabuada(numero);
    resultadosDaTabuada(numero);
  }
}

var tabuadaComNumerosDeterminados = function(){ //Gera tabuadas com números determinados. Ex: 3, 15, 1, 20
  var valores = [];
  var valor   = undefined;


  while(valor != "gerar" && valor != "sair" && valor != "cancelar"){//Checa se valor é != de 'gerar', 'sair' e 'cancelar'   
    valor = prompt("Digite o número da tabuada");                   //Se sim, numeroInicial recebe o valor via prompt
    valor = valor.toLocaleLowerCase();                              //e valor é = ao valor atual do numeroInicial

    if(valor == "gerar"){ //Se var valor for igual a string gerar
      continue;           //Pula a próxima instrução, ou seja, valor não é adicionado ao array valores
    }

    valores.push(valor); //Adiciona valor na última posição do array Valores
  }

  if(valor != "sair" && valor != "cancelar"){
    //Senão, imprime cada valor contido no Array valores
    for(i = 0; i < valores.length; i++){ 
      tituloDaTabuada(valores[i]);
      resultadosDaTabuada(valores[i]);
    }  

    armazenaHistorico(valores.toString());
  }
}